runInCScript
'-----------------------------------------------------------------
argn = Wscript.Arguments.count
if argn = 0 then 
    Wscript.Echo "Too few arguments!" & vbcrlf
    usage 
    Wscript.quit;
end if
arg1 = Wscript.Arguments.Item(0)
if arg1 = "-h" then
    usage
end if
set fso = CreateObject("Scripting.FileSystemObject")
' convert one file
if fso.fileExists(arg1) then
    if argn = 1 then
        desFile = let(arg1, inStrRev(arg1, ".")) & "csv"
        convertOneFile arg1, desFile
        Wscript.quit
    end if
    arg2 = wscript.arguments.item(1)
    convertOneFile arg1, arg2
    wscript.quit
end if
' convert files in a directory
if fso.folderExists(arg1) then
    ext = ".xlsx"
    if argn = 2 then
        arg2 = wscript.arguments.item(1)
        ext = arg2
    end if
    convertDir arg1, ext
end if
'-----------------------------------------------------------------
sub convertOneFile(scrFile, desFile)
    wscript.echo "Converting " & srcFile & " ..."
    '------------------------------------------------------------
    Dim oExcel, oBook, fso
    set fso = createObject("Scripting.FileSystemObject")
    set oExcel = createObject("Excel.Application")
    set oBook = oExcel.workbooks.open(scrFile)
    oExcel.visible = false
    oExcel.displayAlerts = false
    oBook.saveAs desFile, 6
    wscript.echo "Done Converting " & srcFile
    wscript.echo "             to " & desFile & vbcrlf
    oBook.close false
    oExcel.quit
    delayWindowClose 5000
end sub
'-----------------------------------------------------------------
sub convertDir(dir, ext)
    wscript.echo "Opening Excel application ..." & vbcrlf
    '------------------------------------------------------------
    dim fso, folder, files, file, desFile, oExcel, oBook
    set fso = createObject("Scripting.FileSystemObject")
    set folder = fso.getFolder(dir)
    set files = folders.files
    set oExcel = createObject("Excel.Application")
    oExcel.visible = false
    oExceldisplayAlerts = false
    ext = lcase(ext)
    extLenght = len(ext)
    for each file in files
        if left(file.name, 1) <> "~" and lcase(right(file.name, extLength)) = ext then
            wscript.echo "Converting " & file & " ..."
            desFile = left(file, inStrRev(file, ".")) & "csv"
            set oBook = oExcel.workbooks.open(file)
            ' save as csv
            ' xlFileFormat 6 is xlCSV
            oBook.saveAs desFile, 6
            wscript.echo "Done converting " & file
            wscript.echo "             to " & desFile & vbcrlf
            oBook.close
        end if
    next
    wscript.echo "Done converting all Excel files." & vbcrlf
    '---------------- clean up ---------------------------------
    set oBook = nothing
    set oExcel = nothing
    set fso = nothing
    set folder = nothing
    set files = nothing
    set file = nothing
    set desFile = nothing
    '------------------------------------------------------------
    delayWindowClose 5000
end sub
'-----------------------------------------------------------------
Sub runInCScript
    Dim Arg, Str
    If Not LCase(Right(WScript.FullName, 12)) = "\cscript.exe" Then
        For Each Arg In WScript.Arguments
        If InStr(Arg, " ") Then Arg = """" & Arg & """"
        Str = Str & " " & Arg
    Next
    CreateObject("WScript.Shell").Run _
        "cscript //nologo """ & _
        WScript.ScriptFullName & _
        """ " & Str
    WScript.Quit
    End If
End Sub
sub delayWindowClose(n)
    wscript.echo "Window automatically close in " & n/1000 " second(s) ..."
    wscript.sleep n
end sub
sub usage 
    wscript.echo "Convert an Excel File or Excel files in a directory to CSV files."
    wscript.echo "Files start with ""~"" are ignored." & vbcrlf
    wscript.echo "Syntax: excel2csv.vbs excelFile [csvFile]"
    wscript.echo "        excel2csv.vbs directory [extension]" & vbcrlf
    wscript.echo "Note that currently no warning is shown when overwriting existing files."
    wscript.echo "Do not open other Excel applications when this programming is running." & vbcrlf
    delayWindowClose 60000
end sub
