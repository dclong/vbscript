OPTION EXPLICIT

DIM strCodeToExec
strCodeToExec = "msgbox 123"

DO WHILE strCodeToExec<>""
	strCodeToExec = InputBox("Enter VBS code to execute", "WSH Prompt", strCodeToExec)
	IF strCodeToExec<>"" THEN
	on error resume next
	
		execute(strCodeToExec)
	
		IF err.Number<>0 THEN 
			call msgbox("Error #" & err.number & vbCRLF & err.description, vbExclamation, "Run Error")
		END IF

	on error goto 0
	END IF
LOOP


